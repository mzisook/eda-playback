var edaFiles;

var counter = 0;
var PERIOD = .3;
var AMPLITUDE = 100;
var FPS = 100; 

var setupVisualization = function(files) {
	edaFiles = files;
	$("#viewer").empty();
	var height = $("#viewer").height()/edaFiles.length;
	var width = $("#viewer").width(); 
	for (var i = 0; i < edaFiles.length; i++) {
		var eda = edaFiles[i];
		//names eda file object
		var id = "EDA_"+i;
	 
		eda.id = id;
		$("#viewer").append($("<div>").attr("id",id).addClass("row"));
		var divHere = "#"+eda.id;		
		
		var vis = d3.select(divHere).append("svg")
			.attr("class","viz-display")
			.attr("width", width) 
			.attr("height", height);
		
	
		eda.vis = vis; 

		
	}
	//video setup
	var player =_V_($("video")[0]);
	
	/* sets up an event in the video every time the playhead moves, figures out a date object representing the current time in the video, loops through EDA and tries to render visualization by giving it the EDA object and current video time	*/
	player.addEvent("timeupdate", function(e) {
	
				if($("#" + this.id).attr("data-start-time")){
					var videoStart = new Date(parseInt($("#" + this.id).attr("data-start-time")));
				}
				else {
	
					var startTime = $("#" + this.id).find("video").attr("filename");
					//01-17-2013 09:03:45.mp4
					var date = startTime.split(" ")[0].split("-").map(parseInt);
					var time = startTime.split(" ")[1].split(".m")[0].split("_").map(parseFloat);
					console.log(date + " " + time);
					var videoStart = new Date(date[0], date[1]-1, date[2], time[0], time[1], Math.floor(time[2]),(time[2]-Math.floor(time[2]))*1000.0 );
					$("#" + this.id).attr("data-start-time", videoStart.valueOf());
				}
				
				console.log("Current Video Time: " + this.currentTime());
				var delta = new TimeDelta(this.currentTime()*1000);
				var currentVideoTime = videoStart.add(delta);
				for (var n = 0; n < edaFiles.length; n++) {
					var g = edaFiles[n];
					//console.log("g : "); 
					//console.log(g); 
						renderVisualization(g, currentVideoTime);
//					try {
//						renderVisualization(g, currentVideoTime);
//					}
//					catch (error) {
//						console.log(error);
//					}
					
				}
				
	
	});
	
	//Resize video
	player.width(480);
	player.height((9/16)*480); 
	

};


var renderVisualization = function(eda, time){
	console.log(eda); 
	//var div = "#" + eda.id + " svg";
	var div = eda.vis; 
	console.log("Rendering for " + eda.filename + " at time " + time);
	var index = eda.offsetForTime(time);

	
	setInterval(function(){altUpdatePulsingCircle(eda.vis, eda.data.EDA[index]);},PERIOD*FPS);
	//updatePulsingCircle(eda.vis, eda.data.EDA[index]);
	//eda.data.EDA.slice(index-eda.sampleRate*120, index);
	
	//$(div).html("<p>" + eda.filename + " | " + "EDA: "+eda.data.EDA[index]  + " | X: "+eda.data.X[index]+ " | Y: "+eda.data.Y[index]+ " | Z: "+eda.data.Z[index]+ "</p>");

};

var setupPulsingCircle = function(svg) {
	svg.selectAll(svg.childNodes).remove(); // Clears out any circles or other stuff 
	var height = svg.attr("height"); 
	var width = svg.attr("width");  
	
	svg
	  	.attr("class","viz-display")
	  	.append("rect")
	  	.attr("x", 0)
	  	.attr("y",0)
		.attr("width", width)
		.attr("height", height) 
	  	.style("fill", "#003399");
	    //.attr("width", w) 
	    //.attr("height", h);
	  
	    	
	var filter = svg.append("filter")
		.attr("id","blur")
		.append("feGaussianBlur")
		.attr("stdDeviation","10");
		
	
	
	var circle = svg.append("circle")
		//.style("fill","#898dad")
		//.attr("class", "breathing-circle")
		.style("fill", "#535b9a")
		.attr("cx", svg.attr("width")/2)
		.attr("cy", svg.attr("height")/2)
		//.attr("filter","url(#blur)")
		.attr("r", 10) ;
	

};


var updatePulsingCircle = function(svg, edaValue){
 		console.log("svg = "); 
		console.log(svg); 
		setupPulsingCircle(svg);
		var height = svg.attr("height"); 
		var width = svg.attr("width");  
		var r = edaValue * height/6; 
				
		console.log("height = " + height); 
		
		svg.selectAll("circle").attr("r",r);
		
			
			
}


function altUpdatePulsingCircle(svg, edaValue){
	setupPulsingCircle(svg);

	AMPLITUDE = svg.attr("height");
	PERIOD = 1 - edaValue; 
	
	
    var r = Math.abs(Math.sin(counter/(PERIOD*FPS))*AMPLITUDE);
	
    svg.selectAll("circle").attr("r",r);
    
    //console.log("baselineRunningAverage = " + baselineRunningAverage + "  runningAverage = " + runningAverage ); 
    
//    if(runningAverage > baselineRunningAverage*1.05){
//    	//console.log("red"); 
//    	circle.style("fill", "#f80101");
//    }
//    
//    if(runningAverage < baselineRunningAverage*1.05){
//    	//console.log("blue"); 
//    	circle.style("fill", "#535b9a");
//    }
//    
    
    counter++;
   	
}






